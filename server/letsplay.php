<?php 
$myfile = fopen("../".$_GET["key"]."/players.txt", "r") or die("Unable to open file!");
$key = fread($myfile,filesize("../".$_GET["key"]."/players.txt"));
fclose($myfile);

$roomlist = explode('[BRK]',$key); ?>

<html>
<head>
    <title><?php echo $_GET["key"]; ?></title>
    <link rel="stylesheet" href="css/serverView.css">
</head>
<body>
<input type="hidden" id="roomkey" value="<?php echo $roomlist[0]; ?>">
    <?php 
    echo '<h1>Welcome to '.$roomlist[0].'</h1>';
    if(count($roomlist) > 1){
        echo '<h3>In room now:</h3>';
        echo '<div id="roomplayers">';        
        for($i=1;$i<count($roomlist);$i++){
            echo str_replace('[WeAreReadyToDoThis]','',$roomlist[$i]).'<br />';
        }
        echo '</div>';
    }else{
        echo '<h3>Empty room :(</h3>';
    }
    ?>
    <button id="startGameButton">START THE GAME!</button>
</body>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/gameView.js"></script>

</html>