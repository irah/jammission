<?php // Täällä kaikki funktiot mitä voi kuttua kaikkialata

function generateRandomString($length = 4) {
    $characters = 'ABCDEFGHJKLMNPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
} ?>