<?php  header("Access-Control-Allow-Origin: *");

function fileName($length = 10) {
    $characters = 'ABCDEFGHJKLMNPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

$filename = fileName();

$target_dir = "../uploads/";
$target_file = $target_dir . $filename .'.wav';
$uploadOk = 1;

move_uploaded_file($_FILES["data"]["tmp_name"], $target_file);

$desu = shell_exec('python3 /srv/http/pp/py/pitch3.py /srv/http/pp/server/uploads/'.$filename.'.wav');

echo $desu;
?>