<?php include_once 'globals.php'; header("Access-Control-Allow-Origin: *");

// Jos ei oo huonetta niin tee semmonen ja palauta huoneen id
switch ($_POST["handle"]) {
    case 'create_room':
        $newroom = generateRandomString();
        if(!file_exists('../../'.$newroom)){
            mkdir('../../'.$newroom,0777); // Luo huone
            
            $myfile = fopen("../../".$newroom."/players.txt", "w"); // Luo pelaajia varten tiedosto valmiiksi
            fwrite($myfile, $newroom);
            fclose($myfile);
            chmod("../../".$newroom."/players.txt", 0777);
            
            $serverResponse['success'] = 1;
            $serverResponse['key'] = $newroom; // Palauta huoneen 'key' johon palataan
        }else{
            $serverResponse['success'] = 0;
            $serverResponse['reason'] = 'This session is already created';
        }
        exit(json_encode($serverResponse)); // End of create_room
    case 'join_room':
        $roomkey = trim(strtoupper($_POST["key"])); // Room key to uppercase like the rooms are forced to
        $nick = htmlentities(trim($_POST["nick"]));
        $serverResponse['debug'] = $roomkey.' '.$nick;
        if(file_exists('../../'.$roomkey)){ // jos huone löytyy
            
            $myfile = fopen("../../".$roomkey."/players.txt", "a") or $serverResponse['perkele'] = 'asdasdasd';
            $key = fread($myfile,filesize("../../".strtoupper($_POST["key"])."/players.txt"));
            
            if (strpos($key, '[WeAreReadyToDoThis]') == true) {
                $serverResponse['success'] = 0;
                $serverResponse['started'] = 1;
            }else{
                $serverResponse['success'] = 1; // Ilmota että kaikki kivasti
                $serverResponse['started'] = 0;
                fwrite($myfile, '[BRK]'.$nick);
            }
            fclose($myfile);
            
            
            $serverResponse['theroom'] = $roomkey;  // Palautetaan vielä huoneen avain javascript uudelleenohjausta varten
        }else{
            $serverResponse['success'] = 0;
        }
        $serverResponse['debug'] = $roomkey.' '.$nick;
        exit(json_encode($serverResponse)); // End of join_room
    case 'get_room_players':
        $serverResponse['success'] = 0;
        $myfile = fopen("../../".strtoupper($_POST["key"])."/players.txt", "r") or $serverResponse['mode'] = 'offline';
        $key = fread($myfile,filesize("../../".strtoupper($_POST["key"])."/players.txt"));
        fclose($myfile);
        
        $roomlist = explode('[BRK]',$key);
        
        $serverResponse['players'] = '';
        
        for($i=1;$i<count($roomlist);$i++){
            $serverResponse['players'] .= str_replace('[WeAreReadyToDoThis]','',$roomlist[$i]).'<br />';
        }
        
        if(count($roomlist) > 1){
            $serverResponse['success'] = 1;
        }
        
        exit(json_encode($serverResponse));
    case 'start_game':
        $roomkey = trim(strtoupper($_POST["key"])); // Room key to uppercase like the rooms are forced to
        
        $myfile = fopen("../../".$roomkey."/players.txt", "a") or $serverResponse['success'] = 0;;
        fwrite($myfile, '[WeAreReadyToDoThis]');
        fclose($myfile);
        
        $serverResponse['success'] = 1;
        
        exit(json_encode($serverResponse));
    case 'status':
        $serverResponse['success'] = 0;
        $serverResponse['mode'] = 'offline';
        $serverResponse['turn'] = '';
        
//         Ei huonetta tai ei nickiä : offline (default) 
// | Kun ollaan aulassa ja odotellaan: waiting 
// | Kun peli käynnistyy: game 
// 
// ERIKSEEN
// | turn: on tyhjä jos ei vuoroa määrätty, muuten pelaajan nick jonka vuoro (joka on master) aiempien lisäksi
        
        
        $myfile = fopen("../../".strtoupper($_POST["key"])."/players.txt", "r") or $serverResponse['mode'] = 'offline';
        $key = fread($myfile,filesize("../../".strtoupper($_POST["key"])."/players.txt"));
        fclose($myfile);
        


        if (strpos($key, '[WeAreReadyToDoThis]') !== false) {
            if (strpos($key, $_POST["nick"]) !== false) { // Tarkistetaan onko pelaaja listoilla
                $serverResponse['mode'] = 'game';
                $serverResponse['success'] = 1;
                
                if(!file_exists('../../'.strtoupper($_POST["key"]))){ // jos huone löytyy
                    $myfile = fopen("../../".$_POST["key"]."/nakki.txt", "w"); // Luo pelaajia varten tiedosto valmiiksi
                    fwrite($myfile, 'koira');
                    fclose($myfile);
                    chmod("../../".$_POST["key"]."/nakki.txt", 0777);
                    
                    fclose($myfile);
                }
            }else{
                $serverResponse['success'] = 0;
                $serverResponse['mode'] = 'offline';
            }
        }else{
            if (strpos($key, $_POST["nick"]) !== false) { // Tarkistetaan onko pelaaja listoilla
                $serverResponse['mode'] = 'wait';
                $serverResponse['success'] = 1;
            }else{
                $serverResponse['success'] = 0;
                $serverResponse['mode'] = 'offline';
            }
        }
        
        exit(json_encode($serverResponse));
    default:
        
}

?>