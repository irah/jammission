$('#createRoomButton').click(function(){
    $(this).html('Creating room like so hard...');
    
    $.ajax({
    	type: "POST",
    	url: 'tools/manageSession.php',
    	data: { 'handle' : 'create_room' },
    	success: function(data){
            data = JSON.parse(data);
            if(data['success']){
                console.log('Redirectaan '+data['key']);
                window.location.href = 'letsplay.php?key='+data['key'];
            }else{
                console.log('Huone olemassa');
            }
    	},
    	error:function(exception){console.log(exception);}
    });
});