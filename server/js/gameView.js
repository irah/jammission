setInterval(function(){
    $.ajax({
    	type: "POST",
    	url: 'tools/manageSession.php',
    	data: { 'handle' : 'get_room_players' , 'key' : $('#roomkey').val() },
    	success: function(data){
            data = JSON.parse(data);
            if(data['success']){
                $('#roomplayers').html(data['players']);
            }else{
                console.log('Hups');
            }
    	},
    	error:function(exception){console.log(exception);}
    });
}, 10000);

$('#startGameButton').click(function(){
    $.ajax({
    	type: "POST",
    	url: 'tools/manageSession.php',
    	data: { 'handle' : 'start_game' , 'key' : $('#roomkey').val() },
    	success: function(data){
            data = JSON.parse(data);
            // if(data['success']){
            //     $('#roomplayers').html(data['players']);
            // }else{
            //     console.log('Hups');
            // }
    	},
    	error:function(exception){console.log(exception);}
    });
});