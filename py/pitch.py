## PITCH ANALYSIS ##
# 2018 / anlin

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import scipy.io.wavfile as sp_wavfile
import scipy.signal as sp_signal
import numpy.fft as np_fft
#import sys
import json
import argparse


##########################
def debug_plot(data):
    fig, ax = plt.subplots()
    dr=np.arange(0.0, len(data), 1)
    #ax.plot(dr, np.sin(dr)+1)
    ax.plot(dr, data)
    ax.xaxis.set_ticks(np.arange(0,512,8))
    plt.show()

def debug_plot_logx(data):
    fig, ax = plt.subplots()
    dr=np.arange(0.0, len(data), 1)
    #ax.plot(dr, np.sin(dr)+1)
    ax.semilogx()
    ax.plot(dr, data)
    plt.show()

# Float division
def fdiv(a, b):
    return np.true_divide(a, b)

# Reads a .wav file
# Input: path to wave file
# Returns: tuple(sample rate, audio data)
def read_wav(path):
    wav_fs, wav_data = sp_wavfile.read(path)
    wav_data_lr = np.array(wav_data).T
    return wav_fs, wav_data_lr

# Returns the left channel of a stereo file, mono is passed through untouched
# Input: tuple(sample rate, audio data)
# Returns: tuple(sample rate, audio data)
def make_mono(wav):
    if (wav[1].shape[0] == 2):
        return wav[0], wav[1][0]
    else: return wav

# Reads a wave file and returns a mono audio object
# See read_wav
def read_wav_mono(path):
    return make_mono(read_wav(path))


def sample(audio, sample_length_ms=100., overlap=0.0):
    fs = audio[0]
    audio_length_s = len(audio[1])
    sample_length_no_overlap_s = fdiv(sample_length_ms, 1000.)*fs
    sample_length_total_s = sample_length_no_overlap_s*(1.+2.*overlap)
    num_samples = fdiv(len(audio[1]), sample_length_no_overlap_s)
    sample_arr = []
    for x in range(int(num_samples)):
        sample_center = (x+0.5)*sample_length_no_overlap_s
        sample_start = np.maximum(0, sample_center - 0.5*sample_length_total_s)
        sample_end = np.minimum(audio_length_s, sample_center + 0.5*sample_length_total_s)
        sample_arr.append([fs, audio[1][sample_start:sample_end]])
    return sample_arr

# Autocorrelation
# Input: tuple(sample rate, audio data)
# Output: tuple(sample rate, autocorrelation of signal)
def autocorr(audio, length=-1):
    length = length if (length > 0) else len(audio[1])
    fs = audio[0]
    data_clipped = audio[1][:length] # Clip to length
    data_norm = fdiv(data_clipped, data_clipped.max()) # Normalize data
    data_acorr = sp_signal.fftconvolve(data_norm, data_norm[::-1], mode='full') # Perform autocorrelation
    data_acorr = data_acorr[(len(data_acorr)/2):] # Only keep the latter half
    data_acorr[data_acorr < 0] = 0 # Clip to positive only
    rising_edge_offsets = np.where(np.diff(data_acorr) > 0)
    if(len(rising_edge_offsets[0])>0):
        offset = rising_edge_offsets[0][0] # First rising edge after the zero lag
        peak = np.argmax(data_acorr[offset:])+offset # Find the first peak after the zero lag
    else:
        peak = -1
    return fdiv(fs, peak)

def audio_level(audio):
    data_abs = np.absolute(audio[1])
    return np.mean(data_abs)

def filter_samples(audio_arr, threshold):
    sample_means = np.empty(len(audio_arr))
    for s in range(len(audio_arr)):
        sample_means[s] = audio_level(audio_arr[s])
    max_mean = np.max(sample_means)
    #print sample_means
    sample_over_thresh = np.where(sample_means > (threshold*max_mean), True, False)
    return sample_over_thresh

def leading_silence(audio, threshold):
    data = fdiv(np.absolute(audio[1]), np.absolute(audio[1]).max())
    return fdiv(next(i for i,v in enumerate(data) if v>0.1)*1000.,audio[0])


def process_audio(path, sample_length_ms, sample_overlap, sample_valid_threshold, autocorrelation_length):
    a_data = read_wav_mono(path)
    a_leading_silence = leading_silence(a_data, sample_valid_threshold)
    a_samples = sample(a_data, sample_length_ms, sample_overlap) # Chop the full audio clip into samples
    valid_samples = filter_samples(a_samples, sample_valid_threshold)
    a_freqs = []
    for x in range(len(a_samples)):
        a_freqs.append(autocorr(a_samples[x], autocorrelation_length) if valid_samples[x] else -1)
    min_freq = min(f for f in a_freqs if f > 0) # Minimum non-zero
    max_freq = max(a_freqs)

    print json.dumps({
        'sample_length_ms': sample_length_ms, 
        'min_freq_hz': min_freq, 
        'max_freq_hz': max_freq, 
        'leading_silence_ms': a_leading_silence,
        'data': a_freqs
        })
    #debug_plot(a_freqs)

#########################

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("path", type=str, help="Wav file")
    parser.add_argument("-sl", dest="sample_length_ms", type=int, default=50, help="Frequency detection sampling frequency in milliseconds (default 50 ms)")
    parser.add_argument("-st", dest="sample_valid_threshold", type=float, default=0.2, help="Samples with mean amplitude below this threshold are considered invalid (default 0.1)")
    parser.add_argument("-so", dest="sample_overlap", type=float, default=0.0, help="Sample overlap")
    parser.add_argument("-al", dest="autocorrelation_length", type=int, default=-1, help="Autocorrelation sample length, preferably a power of 2")
    args = parser.parse_args()
    process_audio(args.path, args.sample_length_ms, args.sample_overlap, args.sample_valid_threshold, args.autocorrelation_length)

#########################
if __name__=="__main__":
    main()
