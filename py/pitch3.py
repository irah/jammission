## PITCH ANALYSIS ##
# 2018 / anlin

import numpy as np
import scipy as sp
import scipy.io.wavfile as sp_wavfile
import scipy.signal as sp_signal
import json
import argparse

#############################################################

# Float division
def fdiv(a, b):
    return np.true_divide(a, b)

# Reads a .wav file
# Input: path to wave file
# Returns: tuple(sample rate, audio data)
def read_wav(path):
    wav_fs, wav_data = sp_wavfile.read(path)
    wav_data_lr = np.array(wav_data).T
    return wav_fs, wav_data_lr

# Returns the left channel of a stereo file, mono is passed through untouched
# Input: tuple(sample rate, audio data)
# Returns: tuple(sample rate, audio data)
def make_mono(wav):
    if (wav[1].shape[0] == 2):
        return wav[0], wav[1][0]
    else: return wav

# Reads a wave file and returns a mono audio object
# See read_wav
def read_wav_mono(path):
    return make_mono(read_wav(path))

def sample(audio, sample_length_ms=100., overlap=0.0):
    fs = audio[0]
    audio_length_s = len(audio[1])
    sample_length_no_overlap_s = fdiv(sample_length_ms, 1000.)*fs
    sample_length_total_s = sample_length_no_overlap_s*(1.+2.*overlap)
    num_samples = fdiv(len(audio[1]), sample_length_no_overlap_s)
    sample_arr = []
    for x in range(int(num_samples)):
        sample_center = int((x+0.5)*sample_length_no_overlap_s)
        sample_start = int(np.maximum(0, sample_center - 0.5*sample_length_total_s))
        sample_end = int(np.minimum(audio_length_s, sample_center + 0.5*sample_length_total_s))
        sample_arr.append([fs, audio[1][sample_start:sample_end]])
    return sample_arr

# Autocorrelation
# Input: tuple(sample rate, audio data)
# Output: tuple(sample rate, autocorrelation of signal)
def autocorr(audio, length=-1, hf_upper_bound=2000):
    length = length if (length > 0) else len(audio[1])
    fs = audio[0]
    data_clipped = audio[1][:length] # Clip to length
    data_norm = fdiv(data_clipped, data_clipped.max()) # Normalize data
    data_acorr = sp_signal.fftconvolve(data_norm, data_norm[::-1], mode='full') # Perform autocorrelation
    data_acorr = data_acorr[int(len(data_acorr)/2):] # Only keep the latter half
    data_acorr[data_acorr < 0] = 0 # Clip to positive only
    rising_edge_offsets = np.where(np.diff(data_acorr) > 0)
    if(len(rising_edge_offsets[0])>0):
        offset = rising_edge_offsets[0][0] # First rising edge after the zero lag
        peak = np.argmax(data_acorr[offset:])+offset # Find the first peak after the zero lag
    else:
        peak = -1
    freq =  fdiv(fs, peak)
    if (freq >= hf_upper_bound):
        freq = -1
    
    return freq

def audio_level(audio):
    data_abs = np.absolute(audio[1])
    return np.mean(data_abs)

# Input: array(tuple(sample rate, audio data)), threshold
# Output: Boolean array
def filter_samples(audio_arr, threshold):
    sample_means = np.empty(len(audio_arr))
    for s in range(len(audio_arr)):
        sample_means[s] = audio_level(audio_arr[s])
    max_mean = np.max(sample_means)
    sample_over_thresh = np.where(sample_means > (threshold*max_mean), True, False)
    return sample_over_thresh

def freq_gradient_filtering(sample_length_ms, freq_arr, max_gradient):
    for x in range(len(freq_arr)-1):
        gradient = fdiv(np.absolute(freq_arr[x+1]-freq_arr[x]), sample_length_ms)
        if (gradient>=max_gradient):
            freq_arr[x] = -1
    return freq_arr
    

# Input: tuple(sample rate, audio data)
# Output: Leading silence length in milliseconds
def leading_silence(audio, threshold):
    data = fdiv(np.absolute(audio[1]), np.absolute(audio[1]).max())
    return fdiv(next(i for i,v in enumerate(data) if v>0.1)*1000.,audio[0])

#def fill_gaps(freq_array):
#    MAX_GAP = 3
#    zero_count = 0
#    return
#    for i in range(2, len(freq_array):
#        if(freq_array[i]==-1):
#            all_zero = True
#            for x in range(3):
#                if(freq_array[i-(2-x)]!=-1):
#                    all_zero=False
            

        


def process_audio(path, sample_length_ms, sample_overlap, sample_valid_threshold, max_frequency_gradient, autocorrelation_length, autocorrelation_hf_bound):
    a_data = read_wav_mono(path)
    a_leading_silence = leading_silence(a_data, sample_valid_threshold)
    a_samples = sample(a_data, sample_length_ms, sample_overlap) # Chop the full audio clip into samples
    valid_samples = filter_samples(a_samples, sample_valid_threshold)
    a_freqs = []
    for x in range(len(a_samples)):
        a_freqs.append(autocorr(a_samples[x], autocorrelation_length, autocorrelation_hf_bound) if valid_samples[x] else -1)
    min_freq = min(f for f in a_freqs if f > 0) # Minimum non-zero
    max_freq = max(a_freqs)

    a_freqs = freq_gradient_filtering(sample_length_ms, a_freqs, max_frequency_gradient)

    a_freqs.append(-1) #HAck

    print(json.dumps({
        'sample_length_ms': sample_length_ms, 
        'min_freq_hz': min_freq, 
        'max_freq_hz': max_freq, 
        'leading_silence_ms': a_leading_silence,
        'data': a_freqs
        }))



#############################################################

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("path", type=str, help="Wav file")
    parser.add_argument("-sl", dest="sample_length_ms", type=int, default=50, help="Frequency detection sampling frequency in milliseconds (default 50 ms)")
    parser.add_argument("-st", dest="sample_valid_threshold", type=float, default=0.2, help="Samples with mean amplitude below this threshold are considered invalid (default 0.1)")
    parser.add_argument("-sg", dest="sample_max_gradient", type=float, default=10, help="Samples with a frequency gradient greater than this are considered invalid (Hz/ms, default 4.0")
    parser.add_argument("-so", dest="sample_overlap", type=float, default=0.0, help="Sample overlap")
    parser.add_argument("-al", dest="autocorrelation_length", type=int, default=-1, help="Autocorrelation sample length, preferably a power of 2")
    parser.add_argument("-aub", dest="autocorrelation_hf_bound", type=int, default=2000, help="Autocorrelation frequency upper bound (default 2000Hz)")
    args = parser.parse_args()
    process_audio(args.path, args.sample_length_ms, args.sample_overlap, args.sample_valid_threshold, args.sample_max_gradient, args.autocorrelation_length, args.autocorrelation_hf_bound)

#############################################################s
if __name__=="__main__":
    main()