import argparse
import json

def decode_pitch_json(str):
    obj = json.loads(str)
    #print(obj)
    return obj["data"]

# Residual sum of squares
def err_rss(data_x, data_y):
    common_length = min(len(data_x), len(data_y))
    error = 0
    for i in range(common_length):
        error += (data_x[i]-data_y[i])**2
    error/=common_length
    print(json.dumps({"error":error}))

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("json_data_a", type=str, help="Data in JSON format")
    parser.add_argument("json_data_b", type=str, help="Data in JSON format")
    args = parser.parse_args()
    err_rss(decode_pitch_json(args.json_data_a), decode_pitch_json(args.json_data_b))

#############################################################
if __name__=="__main__":
    main()
