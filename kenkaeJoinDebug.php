<html>
<head>
    <title>Pitch Please!</title>
    <link rel="stylesheet" href="server/css/serverView.css">
</head>
<body>
    <h3>Welcome to Pitch Please! Please enter the room key</h3>
    <label>Room key
    <input type="text" id="roomkey" placeholder="Room name" />
    </label><br />
    <label>Your name
    <input type="text" id="nick" placeholder="plz give name" />
    </label><br />
    <button class="btn" id="joinRoom">Join room</button>
    
    <br /><br /><br /><br />
    
    <button id="isItReady">Is this room ready?</button>
</body>

<script src="server/js/jquery-3.3.1.min.js"></script>

</html>

<script>
$('#joinRoom').click(function(){
    $(this).html('Checking for room...');
    
    $.ajax({
    	type: "POST",
    	url: 'server/tools/manageSession.php',
    	data: { 'handle' : 'join_room' , 'key' : $('#roomkey').val() , 'nick' : $('#nick').val() },
    	success: function(data){
            data = JSON.parse(data);
    		console.log(data);
            if(data['success']){
                alert('Hyvin joinasit');
            }else{
                alert('No such room :DDDD');
            }
    	},
    	error:function(exception){console.log(exception);}
    });
});

$('#isItReady').click(function(){
    const that = $(this);
    $(this).html('Checking room state');
    
    $.ajax({
    	type: "POST",
    	url: 'server/tools/manageSession.php',
    	data: { 'handle' : 'ready_check' , 'key' : $('#roomkey').val() },
    	success: function(data){
            data = JSON.parse(data);
    		console.log(data);
            if(data['success']){
                $(that).html('Room is ready');
            }else{
                $(that).html('NOT READY!');
            }
    	},
    	error:function(exception){console.log(exception);}
    });
});
</script>